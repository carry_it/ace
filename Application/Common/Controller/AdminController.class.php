<?php
/**
 * User:    loveblue
 * Date:    2014/11/11
 * Time:    17:16
 * Email:   860760361@qq.com
 */
namespace Common\Controller;

class AdminController extends \Think\Controller {
    public $returnType,$returnMessage,$returnUrl;
    function __construct(){
        $isPublic='pub_'==substr(__ACTION__,0,4);
        if($isPublic || $this->checkLogin()){

        }else{
            if(!('Admin'==__MODULE__ && 'Index'==__CONTROLLER__ && 'pub_login'=='__ACTION__')){

            }
        }
    }
    public function checkLogin(){
        $id=Session('adminId');
        if(empty($id))
            return false;
        else
            return true;
    }
    public function returnResult(){
        if(IS_AJAX){

        }else{

        }
    }
} 